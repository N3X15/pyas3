#from pyparsing import *
#import pyparsing as pyp
_IDENTIFIER=Word(alphas+'_$',alphanums+'_$')
_STRING_LITERAL=dblQuotedString | sglQuotedString
_HEX_LITERAL=Regex(r'0[xX][0-9A-Fa-f]+')
_DECIMAL_LITERAL=Regex(r'(0|\d+)')
_OCTAL_LITERAL=Word('0','01234567') # I think this is supposed to be an O, not a 0.
_FLOAT_LITERAL=Regex(r'\d+(\.\d*)?([eE]\d+)?')
_SL_COMMENT='//'+restOfLine
_ML_COMMENT=cppStyleComment
_REGEXP_LITERAL=Regex(r'/([^/]*)/([a-zA-Z0-9_$]+)?')
