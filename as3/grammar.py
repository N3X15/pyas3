# AUTOMATICALLY GENERATED FROM as3.ebnf
from pyparsing import *

def ActionScript3():
    _IDENTIFIER=Word(alphas+'_$',alphanums+'_$')
    _STRING_LITERAL=dblQuotedString | sglQuotedString
    _HEX_LITERAL=Regex(r'0[xX][0-9A-Fa-f]+')
    _DECIMAL_LITERAL=Regex(r'(0|\d+)')
    _OCTAL_LITERAL=Word('0','01234567') # I think this is supposed to be an O, not a 0.
    _FLOAT_LITERAL=Regex(r'\d+(\.\d*)?([eE]\d+)?')
    _SL_COMMENT='//'+restOfLine
    _ML_COMMENT=cppStyleComment
    _REGEXP_LITERAL=Regex(r'/([^/]*)/([a-zA-Z0-9_$]+)?')
    _notQuiteReservedWord = Forward()
    _expression = Forward()
    _eitherIdentifier = Forward()
    _assignmentExpression = Forward()
    _type = Forward()
    _arrayLiteral = Forward()
    _expressionList = Forward()
    _ident = Forward()
    _typeExpression = Forward()
    _block = Forward()
    _typePostfixSyntax = Forward()
    _identiferLiteral = Forward()
    _numericLiteral = Forward()
    _stringLiteral = Forward()
    _primaryExpression = Forward()
    _unaryExpression = Forward()
    _standardQualifiedName = Forward()
    _statement = Forward()
    _condition = Forward()
    _declaration = Forward()
    _variableType = Forward()
    _variableDeclarator = Forward()
    _includeDirective = Forward()
    _importDirective = Forward()
    _useNamespaceDirective = Forward()
    _modifiers = Forward()
    _LT = Literal('<')
    _LE = Literal('<=')
    _SL_ASSIGN = Literal('<<=')
    _SL = Literal('<<')
    _POST = Literal('.<')
    _DIV = Literal('/')
    _DIV_ASSIGN = Literal('/=')
    _REST = Literal('...')
    _E4X_DESC = Literal('..')
    _DOT = Literal('.')
    _BSLASH = Literal('\\')
    _SEMICOLON = Literal(';')
    _E4X_ATTRI = Literal('@')
    _LOR_ASSIGN = Literal('||=')
    _LAND_ASSIGN = Literal('&&=')
    _LAND = Literal('&&')
    _BAND_ASSIGN = Literal('&=')
    _BAND = Literal('&')
    _LOR = Literal('||')
    _BOR_ASSIGN = Literal('|=')
    _BOR = Literal('|')
    _BXOR_ASSIGN = Literal('^=')
    _BXOR = Literal('^')
    _GT = Literal('>')
    _MOD_ASSIGN = Literal('%=')
    _MOD = Literal('%')
    _STAR = Literal('*')
    _DEC = Literal('--')
    _MINUS_ASSIGN = Literal('-=')
    _MINUS = Literal('-')
    _INC = Literal('++')
    _PLUS_ASSIGN = Literal('+=')
    _PLUS = Literal('+')
    _STRICT_NOT_EQUAL = Literal('!==')
    _NOT_EQUAL = Literal('!=')
    _BNOT = Literal('~')
    _LNOT = Literal('!')
    _STRICT_EQUAL = Literal('===')
    _EQUAL = Literal('==')
    _ASSIGN = Literal('=')
    _COMMA = Literal(',')
    _DBL_COLON = Literal('::')
    _COLON = Literal(':')
    _RBRACE = Literal('}')
    _LBRACE = Literal('{')
    _RBRACK = Literal(']')
    _LBRACK = Literal('[')
    _RPAREN = Literal(')')
    _LPAREN = Literal('(')
    _QUESTION = Literal('?')
    _THIS = Literal('this')
    _SUPER = Literal('super')
    _NEW = Literal('new')
    _NULL = Literal('null')
    _BREAK = Literal('break')
    _CONTINUE = Literal('continue')
    _RETURN = Literal('return')
    _WITH = Literal('with')
    _SET = Literal('set')
    _GET = Literal('get')
    _AS = Literal('as')
    _IS = Literal('is')
    _NAMESPACE = Literal('namespace')
    _USE = Literal('use')
    _DYNAMIC = Literal('dynamic')
    _FALSE = Literal('false')
    _TRUE = Literal('true')
    _INTERFACE = Literal('interface')
    _CLASS = Literal('class')
    _CONST = Literal('const')
    _ELSE = Literal('else')
    _DEFAULT = Literal('default')
    _CASE = Literal('case')
    _SWITCH = Literal('switch')
    _DO = Literal('do')
    _WHILE = Literal('while')
    _IN = Literal('in')
    _EACH = Literal('each')
    _FOR = Literal('for')
    _IMPORT = Literal('import')
    _IF = Literal('if')
    _STATIC = Literal('static')
    _VAR = Literal('var')
    _IMPLEMENTS = Literal('implements')
    _EXTENDS = Literal('extends')
    _FUNCTION = Literal('function')
    _INTERNAL = Literal('internal')
    _PROTECTED = Literal('protected')
    _PRIVATE = Literal('private')
    _PUBLIC = Literal('public')
    _PACKAGE = Literal('package')
    _NATIVE = Literal('native')
    _UNDEFINED = Literal('undefined')
    _TYPEOF = Literal('typeof')
    _DELETE = Literal('delete')
    _INSTANCEOF = Literal('instanceof')
    _VOID = Literal('void')
    _FINALLY = Literal('finally')
    _CATCH = Literal('catch')
    _TRY = Literal('try')
    _THROW = Literal('throw')
    _INCLUDE = Literal('include')
    _OVERRIDE = Literal('override')
    _FINAL = Literal('final')
    # REQUIRES _AS, _BREAK, _CASE, _CATCH, _CLASS, _CONST, _CONTINUE, _DEFAULT, _DELETE, _DO, _ELSE, _EXTENDS, _FALSE, _FINALLY, _FOR, _FUNCTION, _IF, _IMPLEMENTS, _IMPORT, _IN, _INSTANCEOF, _INTERFACE, _INTERNAL, _IS, _NEW, _NULL, _PACKAGE, _PRIVATE, _PROTECTED, _PUBLIC, _RETURN, _SUPER, _SWITCH, _THIS, _THROW, _TRUE, _TRY, _TYPEOF, _USE, _VAR, _VOID, _WHILE, _WITH, _INCLUDE
    _reservedWord = Or([_AS, _BREAK, _CASE, _CATCH, _CLASS, _CONST, _CONTINUE, _DEFAULT, _DELETE, _DO, _ELSE, _EXTENDS, _FALSE, _FINALLY, _FOR, _FUNCTION, _IF, _IMPLEMENTS, _IMPORT, _IN, _INSTANCEOF, _INTERFACE, _INTERNAL, _IS, _NEW, _NULL, _PACKAGE, _PRIVATE, _PROTECTED, _PUBLIC, _RETURN, _SUPER, _SWITCH, _THIS, _THROW, _TRUE, _TRY, _TYPEOF, _USE, _VAR, _VOID, _WHILE, _WITH, _INCLUDE])
    # REQUIRES _reservedWord, _notQuiteReservedWord
    _allKeywords = Or([_reservedWord, _notQuiteReservedWord])
    _notQuiteReservedWord << Or([Literal('native'), Literal('each'), Literal('get'), Literal('set'), Literal('namespace'), Literal('dynamic'), Literal('final'), Literal('override'), Literal('static')])
    # REQUIRES _IDENTIFIER
    _xmlKeyword = _IDENTIFIER
    # REQUIRES _expression
    _e4xFilterPredicate = Group(Literal('[') + _expression + Literal(']'))
    # REQUIRES _eitherIdentifier, _expression
    _e4xAttributeIdentifier = Group(Literal('@') + Or([_eitherIdentifier, Literal('*'), Group(Literal('[') + _expression + Literal(']'))]))
    # REQUIRES _e4xAttributeIdentifier, _e4xFilterPredicate, _eitherIdentifier
    _e4xExpression = Or([Literal('*'), _e4xAttributeIdentifier, _e4xFilterPredicate, _eitherIdentifier])
    # REQUIRES _assignmentExpression
    _encapsulatedExpression = Group(Literal('(') + _assignmentExpression + Literal(')'))
    # REQUIRES _type, _arrayLiteral
    _vectorInitializer = Group(Literal('<') + _type + Literal('>') + _arrayLiteral)
    # REQUIRES _expressionList
    _argumentList = Group(Literal('(') + Optional(_expressionList) + Literal(')'))
    # REQUIRES _ident
    _parameterRestDeclaration = Group(Literal('...') + Optional(_ident))
    # REQUIRES _assignmentExpression
    _parameterDefault = Group(Literal('=') + _assignmentExpression)
    # REQUIRES _ident, _typeExpression, _parameterDefault
    _basicParameterDeclaration = Group(Optional(Literal('const')) + _ident + Optional(_typeExpression) + Optional(_parameterDefault))
    # REQUIRES _basicParameterDeclaration, _parameterRestDeclaration
    _parameterDeclaration = Or([_basicParameterDeclaration, _parameterRestDeclaration])
    # REQUIRES _parameterDeclaration, _COMMA, _parameterDeclaration
    _parameterDeclarationListEntries = Group(_parameterDeclaration + ZeroOrMore(Group(_COMMA + _parameterDeclaration)))
    # REQUIRES _parameterDeclarationListEntries
    _parameterDeclarationList = Group(Literal('(') + Optional(_parameterDeclarationListEntries) + Literal(')'))
    # REQUIRES _parameterDeclarationList, _typeExpression
    _functionSignature = Group(_parameterDeclarationList + Optional(_typeExpression))
    # REQUIRES _functionSignature, _block
    _functionCommon = Group(_functionSignature + _block)
    # REQUIRES _ident, _typePostfixSyntax, _argumentList
    _castExpression = Group(_ident + Optional(_typePostfixSyntax) + _argumentList)
    # REQUIRES _functionCommon
    _functionExpression = Group(Literal('function') + _functionCommon)
    # REQUIRES _assignmentExpression
    _element = _assignmentExpression
    # REQUIRES _identiferLiteral, _numericLiteral, _stringLiteral
    _fieldName = Or([_identiferLiteral, _numericLiteral, _stringLiteral])
    # REQUIRES _fieldName, _element
    _literalField = Group(_fieldName + Literal(':') + _element)
    # REQUIRES _literalField, _literalField
    _fieldList = Group(_literalField + ZeroOrMore(Group(Literal(',') + _literalField)))
    # REQUIRES _LBRACE, _fieldList, _RBRACE
    _objectLiteral = Group(_LBRACE + Optional(_fieldList) + _RBRACE)
    # REQUIRES _assignmentExpression, _assignmentExpression
    _nonemptyElementList = Group(_assignmentExpression + ZeroOrMore(Group(Literal(',') + _assignmentExpression)))
    # REQUIRES _nonemptyElementList
    _elementList = Or([Literal(','), _nonemptyElementList])
    # REQUIRES _STRING_LITERAL
    _stringLiteral << _STRING_LITERAL
    # REQUIRES _HEX_LITERAL, _DECIMAL_LITERAL, _OCTAL_LITERAL, _FLOAT_LITERAL
    _numericLiteral << Or([_HEX_LITERAL, _DECIMAL_LITERAL, _OCTAL_LITERAL, _FLOAT_LITERAL])
    # REQUIRES _elementList
    _arrayLiteral << Group(Literal('[') + Optional(_elementList) + Literal(']'))
    # REQUIRES _numericLiteral, _stringLiteral, _REGEXP_LITERAL
    _constant = Or([Literal('null'), Literal('false'), Literal('true'), _numericLiteral, _stringLiteral, _REGEXP_LITERAL])
    # REQUIRES _IDENTIFIER, _notQuiteReservedWord
    _identiferLiteral << Or([_IDENTIFIER, _notQuiteReservedWord])
    # REQUIRES _primaryExpression
    _newExpression = Group(Literal('new') + _primaryExpression)
    # REQUIRES _identiferLiteral, _constant, _arrayLiteral, _objectLiteral, _encapsulatedExpression, _e4xAttributeIdentifier
    _primaryExpressionHelper = Or([Literal('undefined'), Literal('this'), Literal('super'), _identiferLiteral, _constant, _arrayLiteral, _objectLiteral, _encapsulatedExpression, _e4xAttributeIdentifier])
    # REQUIRES _primaryExpressionHelper
    _primaryExpression << _primaryExpressionHelper
    # REQUIRES _primaryExpression, _functionExpression, _newExpression
    _memberExpression = Or([_primaryExpression, _functionExpression, _newExpression])
    # REQUIRES _IDENTIFIER
    _eitherIdentifier << _IDENTIFIER
    # REQUIRES _argumentList, _expression, _eitherIdentifier, _typePostfixSyntax, _expression, _eitherIdentifier, _e4xAttributeIdentifier, _e4xExpression
    _rightsideLHSX = Or([_argumentList, Group(Literal('[') + _expression + Literal(']')), Group(Literal('.') + _eitherIdentifier), _typePostfixSyntax, Group(Literal('.') + Literal('(') + _expression + Literal(')')), Group(Literal('::') + _eitherIdentifier), Group(Literal('.') + _e4xAttributeIdentifier), Group(Literal('..') + _e4xExpression)])
    # REQUIRES _memberExpression, _rightsideLHSX
    _leftHandSideExpression = Group(_memberExpression + ZeroOrMore(_rightsideLHSX))
    _postfixOperator = Or([Literal('+'), Literal('-')])
    # REQUIRES _leftHandSideExpression, _postfixOperator
    _postfixExpression = Group(_leftHandSideExpression + Optional(_postfixOperator))
    # REQUIRES _postfixExpression, _unaryExpression, _unaryExpression, _unaryExpression, _unaryExpression, _postfixExpression
    _unaryExpressionNotPlusMinus = Or([Group(Literal('del') + _postfixExpression), Group(Literal('void') + _unaryExpression), Group(Literal('typeof') + _unaryExpression), Group(Literal('!') + _unaryExpression), Group(Literal('~') + _unaryExpression), _postfixExpression])
    # REQUIRES _unaryExpression, _unaryExpression, _unaryExpression, _unaryExpression, _unaryExpressionNotPlusMinus
    _unaryExpression << Or([Group(Literal('++') + _unaryExpression), Group(Literal('--') + _unaryExpression), Group(Literal('-') + _unaryExpression), Group(Literal('+') + _unaryExpression), _unaryExpressionNotPlusMinus])
    _multiplicativeOperator = Or([Literal('*'), Literal('/'), Literal('%')])
    # REQUIRES _unaryExpression, _multiplicativeOperator, _unaryExpression
    _multiplicativeExpression = Group(_unaryExpression + ZeroOrMore(Group(_multiplicativeOperator + _unaryExpression)))
    _additiveOperator = Or([Literal('+'), Literal('-')])
    # REQUIRES _multiplicativeExpression, _additiveOperator, _multiplicativeExpression
    _additiveExpression = Group(_multiplicativeExpression + ZeroOrMore(Group(_additiveOperator + _multiplicativeExpression)))
    _shiftOperator = Or([Literal('<<'), Literal('>>'), Literal('>>>'), Literal('<<<')])
    # REQUIRES _additiveExpression, _shiftOperator, _additiveExpression
    _shiftExpression = Group(_additiveExpression + ZeroOrMore(Group(_shiftOperator + _additiveExpression)))
    _relationalOperator = Or([Literal('>'), Literal('>='), Literal('in'), Literal('<'), Literal('<='), Literal('is'), Literal('as'), Literal('instanceof')])
    # REQUIRES _shiftExpression, _relationalOperator, _shiftExpression
    _relationalExpression = Group(_shiftExpression + ZeroOrMore(Group(_relationalOperator + _shiftExpression)))
    _equalityOperator = Or([Literal('==='), Literal('!=='), Literal('!='), Literal('==')])
    # REQUIRES _relationalExpression, _equalityOperator, _relationalExpression
    _equalityExpression = Group(_relationalExpression + ZeroOrMore(Group(_equalityOperator + _relationalExpression)))
    # REQUIRES _equalityExpression, _equalityExpression
    _bitwiseAndExpression = Group(_equalityExpression + ZeroOrMore(Group(Literal('&') + _equalityExpression)))
    # REQUIRES _bitwiseAndExpression, _bitwiseAndExpression
    _bitwiseXorExpression = Group(_bitwiseAndExpression + ZeroOrMore(Group(Literal('^') + _bitwiseAndExpression)))
    # REQUIRES _bitwiseXorExpression, _bitwiseXorExpression
    _bitwiseOrExpression = Group(_bitwiseXorExpression + ZeroOrMore(Group(Literal('|') + _bitwiseXorExpression)))
    # REQUIRES _bitwiseOrExpression, _bitwiseOrExpression
    _logicalAndExpression = Group(_bitwiseOrExpression + ZeroOrMore(Group(Literal('&&') + _bitwiseOrExpression)))
    # REQUIRES _logicalAndExpression, _logicalAndExpression
    _logicalOrExpression = Group(_logicalAndExpression + ZeroOrMore(Group(Literal('||') + _logicalAndExpression)))
    # REQUIRES _assignmentExpression, _assignmentExpression
    _conditionalSubExpression = Group(_assignmentExpression + Literal(':') + _assignmentExpression)
    # REQUIRES _conditionalSubExpression
    _conditionalExpression = Group(Literal('?') + _conditionalSubExpression)
    _assignmentOperator = Or([Literal('='), Literal('*='), Literal('/='), Literal('%='), Literal('+='), Literal('-='), Literal('>>='), Literal('>>>='), Literal('&='), Literal('^='), Literal('|=')])
    # REQUIRES _conditionalExpression
    _assignmentExpression << _conditionalExpression
    # REQUIRES _assignmentExpression, _assignmentExpression
    _expressionList << Group(_assignmentExpression + ZeroOrMore(Group(Literal(',') + _assignmentExpression)))
    # REQUIRES _assignmentExpression
    _expression << _assignmentExpression
    # REQUIRES _standardQualifiedName, _typePostfixSyntax
    _typePostfixSyntax << Group(Literal('post') + Or([_standardQualifiedName, Literal('*')]) + Optional(_typePostfixSyntax) + Literal('>'))
    _reservedNamespace = Or([Literal('public'), Literal('private'), Literal('protected'), Literal('internal')])
    # REQUIRES _IDENTIFIER, _reservedNamespace
    _namespaceName = Or([_IDENTIFIER, _reservedNamespace])
    # REQUIRES _IDENTIFIER, _USE, _DYNAMIC, _NAMESPACE, _IS, _AS, _GET, _SET
    _ident << Or([_IDENTIFIER, _USE, _DYNAMIC, _NAMESPACE, _IS, _AS, _GET, _SET])
    # REQUIRES _IDENTIFIER, _notQuiteReservedWord
    _typeSpecifier = Or([_IDENTIFIER, _notQuiteReservedWord, Literal('internal'), Literal('default')])
    # REQUIRES _typeSpecifier, _typeSpecifier
    _standardQualifiedName << Group(_typeSpecifier + ZeroOrMore(Group(Literal('.') + _typeSpecifier)))
    # REQUIRES _standardQualifiedName, _typePostfixSyntax
    _qualifiedName = Group(_standardQualifiedName + Optional(_typePostfixSyntax))
    # REQUIRES _qualifiedName
    _type << Or([_qualifiedName, Literal('void'), Literal('*')])
    # REQUIRES _type
    _typeExpression << Group(Literal(':') + _type)
    # REQUIRES _IDENTIFIER, _SEMICOLON
    _breakStatement = Group(Literal('break') + Optional(_IDENTIFIER) + _SEMICOLON)
    # REQUIRES _IDENTIFIER, _SEMICOLON
    _continueStatement = Group(Literal('continue') + Optional(_IDENTIFIER) + _SEMICOLON)
    # REQUIRES _expression, _SEMICOLON
    _returnStatement = Group(Literal('return') + Optional(_expression) + _SEMICOLON)
    # REQUIRES _IDENTIFIER, _statement
    _labelStatement = Group(_IDENTIFIER + Literal(':') + _statement)
    # REQUIRES _condition, _statement
    _withStatement = Group(Literal('with') + _condition + _statement)
    # REQUIRES _statement, _condition, _SEMICOLON
    _doWhileStatement = Group(Literal('do') + _statement + Literal('while') + _condition + _SEMICOLON)
    # REQUIRES _condition, _statement
    _whileStatement = Group(Literal('while') + _condition + _statement)
    # REQUIRES _statement
    _defaultStatement = Group(Literal('default') + Literal(':') + ZeroOrMore(_statement))
    # REQUIRES _expression, _statement
    _caseStatement = Group(Literal('case') + _expression + Literal(':') + ZeroOrMore(_statement))
    # REQUIRES _LBRACE, _caseStatement, _defaultStatement, _RBRACE
    _switchBlock = Group(_LBRACE + ZeroOrMore(_caseStatement) + ZeroOrMore(_defaultStatement) + _RBRACE)
    # REQUIRES _condition, _switchBlock
    _switchStatement = Group(Literal('switch') + _condition + _switchBlock)
    # REQUIRES _block
    _finallyBlock = Group(Literal('finally') + _block)
    # REQUIRES _IDENTIFIER, _typeExpression
    _catchBlock = Group(Literal('catch') + Literal('(') + _IDENTIFIER + Optional(_typeExpression) + Literal(')'))
    # REQUIRES _block, _catchBlock, _finallyBlock
    _tryStatement = Group(Literal('try') + _block + ZeroOrMore(_catchBlock) + Optional(_finallyBlock))
    # REQUIRES _expression, _SEMICOLON
    _throwStatement = Group(Literal('throw') + _expression + _SEMICOLON)
    # REQUIRES _expressionList
    _forIter = Optional(_expressionList)
    # REQUIRES _expressionList
    _forCond = Optional(_expressionList)
    # REQUIRES _declaration, _expressionList
    _forInit = Optional(Or([_declaration, _expressionList]))
    # REQUIRES _expressionList
    _forInClauseTail = _expressionList
    # REQUIRES _declaration, _IDENTIFIER
    _forInClauseDecl = Or([_declaration, _IDENTIFIER])
    # REQUIRES _forInClauseDecl, _forInClauseTail
    _forInClause = Group(_forInClauseDecl + Literal('in') + _forInClauseTail)
    # REQUIRES _forInit, _forCond, _forIter
    _traditionalForClause = Group(_forInit + Literal(';') + _forCond + Literal(';') + _forIter)
    # REQUIRES _forInClause, _traditionalForClause
    _forClause = Or([_forInClause, _traditionalForClause])
    # REQUIRES _forClause, _statement
    _forStatement = Group(Literal('for') + Literal('(') + _forClause + Literal(')') + _statement)
    # REQUIRES _forInClause, _statement
    _forEachStatement = Group(Literal('for') + Literal('each') + Literal('(') + _forInClause + Literal(')') + _statement)
    # REQUIRES _expression
    _condition << Group(Literal('(') + _expression + Literal(')'))
    # REQUIRES _condition, _statement, _statement
    _ifStatement = Group(Literal('if') + _condition + _statement + Optional(Group(Literal('else') + _statement)))
    # REQUIRES _expressionList, _SEMICOLON
    _expressionStatement = Group(_expressionList + _SEMICOLON)
    # REQUIRES _SEMICOLON
    _emptyStatement = _SEMICOLON
    # REQUIRES _assignmentExpression
    _variableInitializer = Group(Literal('=') + _assignmentExpression)
    # REQUIRES _variableType, _variableDeclarator, _variableDeclarator
    _declaration << Group(_variableType + _variableDeclarator + ZeroOrMore(Group(Literal(',') + _variableDeclarator)))
    # REQUIRES _declaration, _SEMICOLON
    _declarationStatement = Group(_declaration + _SEMICOLON)
    # REQUIRES _IDENTIFIER, _typeExpression, _variableInitializer
    _variableDeclarator << Group(_IDENTIFIER + Optional(_typeExpression) + Optional(_variableInitializer))
    # REQUIRES _VAR, _CONST, _NAMESPACE
    _variableType << Or([_VAR, _CONST, _NAMESPACE])
    # REQUIRES _declarationStatement, _expressionStatement, _ifStatement, _forEachStatement, _forStatement, _whileStatement, _doWhileStatement, _continueStatement, _breakStatement, _returnStatement, _withStatement, _labelStatement, _switchStatement, _throwStatement, _tryStatement, _includeDirective, _importDirective, _useNamespaceDirective, _emptyStatement
    _statement << Or([_declarationStatement, _expressionStatement, _ifStatement, _forEachStatement, _forStatement, _whileStatement, _doWhileStatement, _continueStatement, _breakStatement, _returnStatement, _withStatement, _labelStatement, _switchStatement, _throwStatement, _tryStatement, _includeDirective, _importDirective, _useNamespaceDirective, _emptyStatement])
    # REQUIRES _statement
    _blockEntry = _statement
    # REQUIRES _LBRACE, _blockEntry, _RBRACE
    _block << Group(_LBRACE + ZeroOrMore(_blockEntry) + _RBRACE)
    _accessorRole = Or([Literal('get'), Literal('set')])
    # REQUIRES _accessorRole, _IDENTIFIER, _parameterDeclarationList, _typeExpression, _SEMICOLON
    _interfaceMethodDefinitionBlockEntry = Group(Literal('function') + Optional(_accessorRole) + _IDENTIFIER + _parameterDeclarationList + Optional(_typeExpression) + _SEMICOLON)
    # REQUIRES _accessorRole, _IDENTIFIER, _parameterDeclarationList, _typeExpression, _block
    _classMethodDefinitionBlockEntry = Group(Literal('function') + Optional(_accessorRole) + _IDENTIFIER + _parameterDeclarationList + Optional(_typeExpression) + _block)
    # REQUIRES _variableType, _variableDeclarator, _variableDeclarator, _SEMICOLON
    _fieldDefinitionBlockEntry = Group(_variableType + _variableDeclarator + ZeroOrMore(Group(Literal(',') + _variableDeclarator)) + _SEMICOLON)
    # REQUIRES _IDENTIFIER, _SEMICOLON
    _staticLinkEntry = Group(_IDENTIFIER + _SEMICOLON)
    # REQUIRES _LBRACE, _blockEntry, _RBRACE
    _staticBlockEntry = Group(_LBRACE + ZeroOrMore(_blockEntry) + _RBRACE)
    # REQUIRES _interfaceMethodDefinitionBlockEntry
    _interfaceTypeBlockEntry = _interfaceMethodDefinitionBlockEntry
    # REQUIRES _LBRACE, _interfaceTypeBlockEntry, _RBRACE
    _interfaceTypeBlock = Group(_LBRACE + _interfaceTypeBlockEntry + _RBRACE)
    # REQUIRES _fieldDefinitionBlockEntry, _classMethodDefinitionBlockEntry, _staticLinkEntry
    _classTypeBlockEntry = Or([_fieldDefinitionBlockEntry, _classMethodDefinitionBlockEntry, _staticLinkEntry])
    # REQUIRES _LBRACE, _classTypeBlockEntry, _RBRACE
    _classTypeBlock = Group(_LBRACE + ZeroOrMore(_classTypeBlockEntry) + _RBRACE)
    # REQUIRES _modifiers, _variableType, _variableDeclarator, _variableDeclarator, _SEMICOLON
    _variableDefinition = Group(_modifiers + _variableType + _variableDeclarator + ZeroOrMore(Group(Literal(',') + _variableDeclarator)) + _SEMICOLON)
    # REQUIRES _modifiers, _IDENTIFIER, _parameterDeclarationList, _typeExpression, _block
    _functionDefinition = Group(_modifiers + Literal('function') + _IDENTIFIER + _parameterDeclarationList + Optional(_typeExpression) + Optional(_block))
    # REQUIRES _type, _type
    _interfaceExtendsClause = Group(Literal('extends') + _type + ZeroOrMore(Group(Literal(',') + _type)))
    # REQUIRES _modifiers, _IDENTIFIER, _interfaceExtendsClause, _interfaceTypeBlock
    _interfaceDefinition = Group(_modifiers + Literal('interface') + _IDENTIFIER + Optional(_interfaceExtendsClause) + _interfaceTypeBlock)
    # REQUIRES _type, _type
    _classImplementsClause = Group(Literal('implements') + _type + ZeroOrMore(Group(Literal(',') + _type)))
    # REQUIRES _type
    _classExtendsClause = Group(Literal('extends') + _type)
    # REQUIRES _modifiers, _IDENTIFIER, _classExtendsClause, _classImplementsClause, _classTypeBlock
    _classDefinition = Group(_modifiers + Literal('class') + _IDENTIFIER + Optional(_classExtendsClause) + Optional(_classImplementsClause) + _classTypeBlock)
    _modifier = Or([Literal('dynamic'), Literal('final'), Literal('internal'), Literal('override'), Literal('private'), Literal('protected'), Literal('public'), Literal('static'), Literal('native')])
    # REQUIRES _modifier
    _modifiers << ZeroOrMore(_modifier)
    # REQUIRES _IDENTIFIER, _DBL_COLON, _IDENTIFIER
    _configIdent = Group(_IDENTIFIER + _DBL_COLON + _IDENTIFIER)
    # REQUIRES _IMPORT, _type, _DOT, _STAR, _SEMICOLON
    _importDirective << Group(_IMPORT + _type + Optional(Group(_DOT + _STAR)) + _SEMICOLON)
    # REQUIRES _USE, _NAMESPACE, _IDENTIFIER, _SEMICOLON
    _useNamespaceDirective << Group(_USE + _NAMESPACE + _IDENTIFIER + _SEMICOLON)
    # REQUIRES _INCLUDE, _STRING_LITERAL, _SEMICOLON
    _includeDirective << Group(_INCLUDE + _STRING_LITERAL + _SEMICOLON)
    # REQUIRES _includeDirective, _importDirective, _useNamespaceDirective
    _annotation = Or([_includeDirective, _importDirective, _useNamespaceDirective])
    # REQUIRES _annotation, _classDefinition, _interfaceDefinition, _variableDefinition, _functionDefinition
    _packageBlockEntry = Or([_annotation, _classDefinition, _interfaceDefinition, _variableDefinition, _functionDefinition])
    # REQUIRES _LBRACE, _packageBlockEntry, _RBRACE
    _packageBlock = Group(_LBRACE + ZeroOrMore(_packageBlockEntry) + _RBRACE)
    # REQUIRES _PACKAGE, _type, _packageBlock
    _packageDecl = Group(_PACKAGE + _type + _packageBlock)
    # REQUIRES _packageDecl, _packageBlockEntry
    _asCompilationUnit = Group(_packageDecl + ZeroOrMore(_packageBlockEntry))
    return _asCompilationUnit
