import codecs
from as3.grammar import ActionScript3
from pyparsing import ParseException

def testParsingOf(doc):
    with codecs.open('test.as','w') as f:
        f.write(doc)
    with codecs.open('test.as','r') as f:
        try:
            ActionScript3().parseFile(f)
        except ParseException, err:
            print err.line
            print " "*(err.column-1) + "^"
            print err

testdoc = '''
package test.package.lol {
    public class AClass {

    }
}
'''

testParsingOf(testdoc)
