# Based on the ebnf.py example by Seo Sanghyeon for pyparsing.
# Heavily modified by Rob Nelson to support CPython-style "EBNF" grammar.
#
from pyparsing import *
from buildtools import os_utils, log
from as3.utils import IndentWriter, writeReindentedViaIndentWriter
import os

class SemanticGroup(object):
    label=''
    sep=''
    inList=False
    def __init__(self,contents):
        self.contents = contents
        self.requires=[]
        while self.contents[-1].__class__ == self.__class__:
            self.contents = self.contents[:-1] + self.contents[-1].contents
        for c in self.contents:
            if isinstance(c, SemanticGroup):
                #self.requires+=[x for x in c.requires if not x.startswith("'")]
                self.requires+=c.requires
            elif isinstance(c, basestring):
                #if not c.startswith("'"):
                self.requires+=[c]

    def stringifyChild(self,c):
        #print(repr(c))
        if isinstance(c,basestring):
            return c
        return str(c)

    def __str__(self):
        o = self.sep.join([self.stringifyChild(c) for c in self.contents])
        if self.inList:
            o='[%s]' % o
        return "%s(%s)" % (self.label, o)

class OrList(SemanticGroup):
    label = "Or"
    sep=', '
    inList=True
    pass

class AndList(SemanticGroup):
    label = "Group"
    sep=' + '
    #inList=True

class OptionalGroup(SemanticGroup):
    label = "Optional"
    sep=' + '
    pass

class Atom(SemanticGroup):
    def __init__(self,contents):
        self.requires=[]
        if len(contents) > 1:
            self.rep = contents[1]
        else:
            self.rep = ""
        if isinstance(contents,basestring):
            self.contents = contents
        else:
            self.contents = contents[0]
        if isinstance(self.contents,SemanticGroup):
            self.requires+=self.contents.requires
        else:
            if not self.contents.startswith("'") and not self.contents.endswith("'"):
                self.contents='_'+self.contents
            self.requires+=[self.contents]
    def isLiteral(self):
        o = str(self.contents)
        return o.startswith("'") and o.endswith("'")
    def __str__(self):
        o = str(self.contents)
        if o.startswith("'") and o.endswith("'"):
            o='Literal(%s)' % o
        if self.rep == '+':
            o = 'OneOrMore(%s)' % o
        if self.rep == '*':
            o = 'ZeroOrMore(%s)' % o
        return o

def makeGroupObject(cls):
    def groupAction(s,l,t):
        try:
            return cls(t[0].asList())
        except:
            return cls(t)
    return groupAction
def wrapLiteral(cls):
    def wrapAction(s,l,t):
        return cls(t)
    return wrapAction

def pyBNF():
    # bnf punctuation
    LPAREN = Suppress("(")
    RPAREN = Suppress(")")
    LBRACK = Suppress("[")
    RBRACK = Suppress("]")
    COLON  = Suppress(":")
    ALT_OP = Suppress("|")

    # bnf grammar
    ident = Word(alphanums+"_")
    bnfToken = (Word(alphanums+"_") + ~FollowedBy(":"))
    repSymbol = oneOf("* +")
    bnfExpr = Forward()
    optionalTerm = Group(LBRACK + bnfExpr + RBRACK).setParseAction(makeGroupObject(OptionalGroup))
    #literalString = quotedString.addParseAction(makeGroupObject(LiteralString))
    bnfTerm = ( (bnfToken | quotedString | optionalTerm | ( LPAREN + bnfExpr + RPAREN )) + Optional(repSymbol) ).setParseAction(makeGroupObject(Atom))
    andList = Group(bnfTerm + OneOrMore(bnfTerm)).setParseAction(makeGroupObject(AndList))
    bnfFactor = andList | bnfTerm
    orList = Group( bnfFactor + OneOrMore( ALT_OP + bnfFactor ) ).setParseAction(makeGroupObject(OrList))
    bnfExpr << ( orList | bnfFactor )
    bnfLine = (ident + COLON + bnfExpr)

    bnfComment = "#" + restOfLine
    #import pyparsing as pyp
    longComment = cppStyleComment

    # build return tokens as a dictionary
    bnf = Dict(OneOrMore(Group(bnfLine)))
    bnf.ignore(bnfComment)
    bnf.ignore(longComment)
    return bnf
os_utils.ensureDirExists('as3')
header=[]
NEEDS_FORWARD=[]
DEFINED=[]
KNOWN_LITERALS={}
with open('as3/grammar.header.py','r') as hf:
    for line in hf:
        header += [line]
        if line.startswith('_'):
            var,_ = line.split('=',1)
            DEFINED+=[var]
with open('as3/grammar.py','w') as wf:
    with open('as3.ebnf','r') as f:
        w = IndentWriter(wf,indent_chars='    ')
        w.writeline('# AUTOMATICALLY GENERATED FROM as3.ebnf')
        w.writeline('from pyparsing import *')
        w.writeline('')
        with w.writeline('def ActionScript3():'):
            writeReindentedViaIndentWriter(w,header)
            o = pyBNF().parseFile(f)
            for k,v in reversed(o):
                orig_key=k
                k='_'+k
                for ik in v.requires:
                    if ik not in DEFINED and not ik.startswith("'") and ik not in NEEDS_FORWARD:
                        NEEDS_FORWARD+=[ik]
                if isinstance(v, Atom) and v.isLiteral():
                    KNOWN_LITERALS[str(v.contents)]=orig_key
                DEFINED+=[k]
            for k in NEEDS_FORWARD:
                if k not in DEFINED:
                    w.writeline('# NEVER DEFINED!')
                    log.warn('%s is never defined!',k)
                w.writeline('{} = Forward()'.format(k))
            for k,v in reversed(o):
                orig_key=k
                k='_'+k
                for req in v.requires:
                    if req in KNOWN_LITERALS:
                        log.warn('%s: You should probably change %s to %s.', orig_key,req,KNOWN_LITERALS[req])
                var_reqs=[x for x in v.requires if not x.startswith("'")]
                if len(var_reqs)>0:
                    w.writeline('# REQUIRES {}'.format(', '.join(var_reqs)))
                if k in NEEDS_FORWARD:
                    w.writeline('{} << {}'.format(k,v))
                else:
                    w.writeline('{} = {}'.format(k,v))
            w.writeline('return _asCompilationUnit')
